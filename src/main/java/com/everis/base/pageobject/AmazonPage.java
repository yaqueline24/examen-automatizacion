package com.everis.base.pageobject;


/**
 * @author jovallep
 */
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.amazon.com/")
public class AmazonPage extends PageObject {

    @FindBy(xpath = "/html/body/header/div")
    public WebElementFacade btnCuentasyListas;

    @FindBy(xpath = "/html/body/div/div[1]/div[2]/div[3]/span/span[1]")
    public WebElementFacade btnCrearCuenta;

    @FindBy(xpath = "//*[@id=\"ap_customer_name\"]")
    public WebElementFacade textoNombre;

    @FindBy(xpath = "//*[@id=\"ap_email\"]")
    public WebElementFacade textoCorreo;

    @FindBy(xpath = "//*[@id=\"ap_password\"]")
    public WebElementFacade textoClave;

    @FindBy(xpath = "//*[@id=\"ap_password_check\"]")
    public WebElementFacade textoClave;

    @FindBy(xpath = "//*[@id=\"ap_register_form\"]/div/div/div[2]/span/input")
    public WebElementFacade textoCrearCuenta;

}
