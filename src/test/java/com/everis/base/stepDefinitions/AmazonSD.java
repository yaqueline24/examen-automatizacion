package com.everis.base.stepDefinitions;

import com.everis.base.steps.AmazonSteps;
import com.everis.base.steps.AmazonSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.apache.xpath.operations.String;

/**
 * @author jovallep
 */
public class AmazonSD {

    @Steps
    public AmazonSteps amazonSteps;

    @Given("cliente accede a la web de Amazon")
    public void clienteAccedeALaWebDeAmazon() { amazonSteps.open();
    }

    @When("selecciona cuentas y listas")
    public void seleccionaCuentasYListas() { amazonSteps.selectAccountAndListCreation();
    }

    @And("presiona boton crear tu cuenta de Amazon")
    public void presionaBotonCrearTuCuentaDeAmazon() { amazonSteps.selectAccountCreation();
    }

    @And("escribe nombre en campo Tu nombre {string}")
    public void escribeNombreEnCampoTuNombre(String var) { amazonSteps.writeName(var);
    }

    @And("escribe email de cliente {string}")
    public void escribeEmailDeCliente(String var) { amazonSteps.writeEmail(var);
    }

    @And("escribe contrasena de cliente {string}")
    public void escribeContrasenaDeCliente(String var) { amazonSteps.writePassword(var);
    }

    @And("escribe contrasena de cliente nuevamente {string}")
    public void escribeContrasenaDeClienteNuevamente(String var) { amazonSteps.writePassword2(var);
    }

    @Then("validar el texto del Boton {string}")
    public void validarElTextoDelBoton(String texto) { amazonSteps.validarTextoCrearCuenta(texto);
    }


}
