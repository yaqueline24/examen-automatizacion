package com.everis.base.steps;

import com.everis.base.pageobject.MercadoLibrePage;
import net.thucydides.core.annotations.Step;
import org.apache.xpath.operations.String;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jovallep
 */
public class AmazonSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmazonSteps.class);

    private Amazon page;

    @Step
    public void crearCliente() {
        LOGGER.info("Ingresa la información del cliente");
        selectAccountCreation();
        writeName("111");
        writeEmail("asd@asd.com");
        writePassword("123456");
        writePassword2("123456");
    }

    public void open() {
        LOGGER.info("Abre la pagina web");
        page.open();
    }

    public void selectAccountAndListCreation() {

        LOGGER.info("Selecciona la opcion Cuentas y Listas");
        page.btnCuentasyListas.click();
    }

    public void selectAccountCreation() {

        LOGGER.info("Selecciona el boton Crea tu cuenta de Amazon");
        page.btnCrearCuenta.click();
    }

    public void writeName(String var) {
        LOGGER.info("Escribe el Nombre completo");
        page.textoNombre.sendKeys(var);
    }

    public void writeEmail(String var) {
        LOGGER.info("Escribe el Correo electronico");
        page.textoCorreo.sendKeys(var);
    }

    public void writePassword(String var) {
        LOGGER.info("Escribe la contrasena");
        page.textoClave.sendKeys(var);
    }

    public void writePassword2(String var) {
        LOGGER.info("Repite la contrasena");
        page.textoClave.sendKeys(var);
    }

    public void validarTextoCrearCuenta(String texto) {
        LOGGER.info("texto:" + page.textoCrearCuenta.getText());
        page.textoCrearCuenta.isDisplayed();
        Assert.assertEquals(texto, page.textoCrearCuenta.getText());
    }





}
