@web
Feature: Registro en Amazon

  @esc_01
  Scenario: Cliente ingresa su información 1
    Given cliente accede a la web de Amazon
    When selecciona cuentas y listas
    And  presiona boton crear tu cuenta de Amazon
    And escribe nombre en campo Tu nombre "Juan Perez"
    And escribe email de cliente "juanperez@gmail.com"
    And escribe contrasena de cliente "123456"
    And escribe contrasena de cliente nuevamente "123456"
    Then validar el texto del boton "Crea tu cuenta de Amazon"

